﻿#pragma warning disable 0649
using System;
using NSTranslateSystem;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NSTranslateSystem
{
    /// <summary>
    /// Clase que se adjunta a cualquier text de la interfaz para que este se pueda traducir
    /// </summary>
    public class TextTraductor : AbstractTextTraductor
    {
        #region private methods
        
        protected override void Traducir()
        {
            var tmpTextMeshProUGUI = GetComponent<TextMeshProUGUI>();

            if (tmpTextMeshProUGUI)
            {
                var traduccion = DiccionarioIdiomas.Instance.Traducir(name);
                var textoConFormato = new StringBuilder(traduccion);
                tmpTextMeshProUGUI.SetText(textoConFormato);
                LayoutRebuilder.ForceRebuildLayoutImmediate(GetComponent<RectTransform>());
            }
        }
        #endregion
    }
}