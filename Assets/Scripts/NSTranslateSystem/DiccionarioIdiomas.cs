﻿#pragma warning disable 0649
#pragma warning disable 0618
using System;
using NSSingleton;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using NSScriptableEvent;
using UnityEngine;
using UnityEngine.Networking;

namespace NSTranslateSystem
{
    /// <summary>
    /// Carga el idioma desde archivos XML
    /// </summary>
    public class DiccionarioIdiomas : AbstractSingleton<DiccionarioIdiomas>
    {
        #region members

        /// <summary>
        ///  Diccionario para las traducciones al idioma Español
        /// </summary>
        public Dictionary<string, string> dictionaryTranslateBaseSimulator = new Dictionary<string, string>();

        /// <summary>
        /// Idioma seleccionado actualmente
        /// </summary>
        private IdiomsList idiomaActual;

        [SerializeField] private string[] arrayTextosTraduccion =
        {
            "TextosBaseSimulador",
            "TextosSituacion1",
            "TextosSituacion2",
            "TextosSituacion3",
            "TextosSituacion4"
        };

        /// <summary>
        /// Para notificar cuando un diccionario fue cargado
        /// </summary>
        public ScriptableEvent seOnDictionaryLoaded;

        #endregion

        #region Events

        [Header("Events")] [SerializeField] private ScriptableEvent seOnButtonSelectSpanishAsIdiom;

        [SerializeField] private ScriptableEvent seOnButtonSelectEnglishAsIdiom;

        #endregion

        #region delegates

        /// <summary>
        /// delegado al que se suscriben el metodo que traduce un text por su object name
        /// </summary>
        public delegate void delegateTraducirporNombreDelObjeto();

        public delegateTraducirporNombreDelObjeto DLTraducirForNameObj = delegate { };

        #endregion

        #region MonoBehaviour

        private void OnEnable()
        {
            seOnButtonSelectSpanishAsIdiom.Subscribe(SetIdiomToSpanish);
            seOnButtonSelectEnglishAsIdiom.Subscribe(SetIdiomToEnglish);
        }

        private void OnDisable()
        {
            seOnButtonSelectSpanishAsIdiom.Unsubscribe(SetIdiomToSpanish);
            seOnButtonSelectEnglishAsIdiom.Unsubscribe(SetIdiomToEnglish);
        }

        #endregion

        #region private methods

        private void SetIdiomToSpanish()
        {
            SetIdiom(IdiomsList.Spanish);
        }

        private void SetIdiomToEnglish()
        {
            SetIdiom(IdiomsList.English);
        }


        /// <summary>
        /// Debug para encontrar facilmente el string que no tiene traduccion en los diccionarios
        /// </summary>
        private void TrackingTextoSinTraduccion(string argStringParaTraducir)
        {
            Debug.LogWarning("String sin traduccion : " + argStringParaTraducir);
        }

        /// <summary>
        /// metodo que llena los diccionarios con las traducciones del xml
        /// </summary>
        private void CargarTextosIdioma()
        {
            dictionaryTranslateBaseSimulator.Clear();
            StartCoroutine(CouLeerTraduccionesXML());
        }

        /// <summary>
        /// Courutina para leer las traducciones de un XML
        /// </summary>
        /// <param name="argXMLFileName">Nombre del archivo de texto.</param>
        /// <param name="argDiccionario">Diccionario al que se le van a agregar las traducciones.</param>
        private IEnumerator CouLeerTraduccionesXML()
        {
            var tmpFolderIdiom = "";

            switch (idiomaActual)
            {
                case IdiomsList.Spanish:
                    tmpFolderIdiom = "Espanish";
                    break;

                case IdiomsList.English:
                    tmpFolderIdiom = "English";
                    break;

                case IdiomsList.Portugues:
                    tmpFolderIdiom = "Portugues";
                    break;
            }

            foreach (var tmpNameFileXml in arrayTextosTraduccion)
            {
                var tmpDirectoryFixed = Path.Combine(Application.streamingAssetsPath, "FilesTraductionXml", tmpFolderIdiom, tmpNameFileXml + ".xml");

                XmlDocument reader = new XmlDocument();

#if UNITY_ANDROID
                tmpDirectoryFixed = "jar:file://" + Application.dataPath + "!/assets/" + "FilesTraductionXml/" + tmpFolderIdiom + "/" + tmpNameFileXml + ".xml";
                WWW wwwfile = new WWW(tmpDirectoryFixed);
                yield return wwwfile;

                try
                {
                    if (!string.IsNullOrEmpty(wwwfile.error))
                        Debug.Log("no se encontro el archivo");

                    reader.LoadXml(wwwfile.text);
                }
                catch (Exception argException)
                {
                    Debug.Log("No existe el archivo de traduccion " + argException);
                }
#elif UNITY_WEBGL
                UnityWebRequest www = UnityWebRequest.Get(tmpDirectoryFixed);
                yield return www.SendWebRequest();
                reader.LoadXml(www.downloadHandler.text);
#else
                reader.LoadXml(System.IO.File.ReadAllText(tmpDirectoryFixed));
                yield return null;
#endif

                try
                {
                    var tmpListNodesXML = reader.ChildNodes[0].ChildNodes;

                    for (int i = 0; i < tmpListNodesXML.Count; i++)
                    {
                        var tmpNewKeyDictionary = tmpListNodesXML[i].Name;
                        if (dictionaryTranslateBaseSimulator.ContainsKey(tmpNewKeyDictionary))
                            Debug.Log("La Key con el nombre <color=red> : " + tmpNewKeyDictionary + " </color> esta duplicada.");
                        else
                            dictionaryTranslateBaseSimulator.Add(tmpNewKeyDictionary, tmpListNodesXML[i].InnerXml);
                    }
                }
                catch (Exception argException)
                {
                    Debug.Log("No existe el archivo de traduccion " + argException);
                }
            }

            yield return null;
            seOnDictionaryLoaded.ExecuteEvent();
        }

        #endregion

        #region public methods

        /// <summary>
        /// Metodo por medio del cual se solicita la traduccion de un string
        /// </summary>
        /// <param name="argNombreTextParaTraducir">Nombre de la etiqueta que se quiere traducir</param>
        public string Traducir(string argNombreTextParaTraducir)
        {
            if (dictionaryTranslateBaseSimulator.ContainsKey(argNombreTextParaTraducir))
                return dictionaryTranslateBaseSimulator[argNombreTextParaTraducir];

            TrackingTextoSinTraduccion(argNombreTextParaTraducir);
            return "La etiqueta : <" + argNombreTextParaTraducir + "> no existe en el XML, <color=\"red\">porfavor crearla y asignarle texto.";
        }

        /// <summary>
        /// Metodo por medio del cual se solicita la traduccion de un string, con argumentos adicionales para remplazar en el texto
        /// </summary>
        /// <param name="argNombreTextParaTraducir">Nombre de la etiqueta que se quiere traducir</param>
        public string Traducir(string argNombreTextParaTraducir, params string[] argTagsParaRemplazar)
        {
            var tmpTextTraducido = Traducir(argNombreTextParaTraducir);

            for (int i = 0; i < argTagsParaRemplazar.Length; i++)
                tmpTextTraducido = tmpTextTraducido.Replace("{" + i + "}", Traducir(argTagsParaRemplazar[i]));

            return tmpTextTraducido;
        }

        /// <summary>
        /// Asigna un idioma
        /// </summary>
        /// <param name="argIdioma">Idioma que se usara</param>
        public void SetIdiom(IdiomsList argIdioma)
        {
            idiomaActual = argIdioma;
            CargarTextosIdioma();
            DLTraducirForNameObj();
        }

        #endregion
    }

    public enum IdiomsList
    {
        Spanish,
        English,
        Portugues
    }
}