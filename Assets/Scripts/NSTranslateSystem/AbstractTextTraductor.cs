﻿#pragma warning disable 0649
using System;
using NSTranslateSystem;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NSTranslateSystem
{
    /// <summary>
    /// Clase que se adjunta a cualquier text de la interfaz para que este se pueda traducir
    /// </summary>
    public abstract class AbstractTextTraductor : MonoBehaviour
    {
        #region monobehaviour

        private void OnEnable()
        {
            Traducir();
        }
        
        private void Start()
        {
            DiccionarioIdiomas.Instance.DLTraducirForNameObj += Traducir;
        }

        private void OnDisable()
        {
            if(DiccionarioIdiomas.SingletonExist)
                DiccionarioIdiomas.Instance.DLTraducirForNameObj -= Traducir;

        }
        #endregion

        #region private methods

        /// <summary>
        /// identifica el tipo de texto y lo traduse
        /// </summary>
        protected abstract void Traducir();
        #endregion
    }
}